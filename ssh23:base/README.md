# M06. ADMINISTRACIÓ AVANÇADA DE SISTEMES OPERATIUS

1. **Crear la imatge edtasixm06/ssh23:base amb els requisits següents:**

* Basada en pam23:ldap de manera que permeti l’autenticació d’usuaris unix locals i usuaris LDAP.
* Els containers han de poder executar-se en detach.
* El servei SSH s’ha de propagar al host al port 2022.
* Generar el README pertinent i pujar-ho tot al git i a Dockerhub.

2. **Aprofitar per entendre els conceptes:**

* Procés d’instal·lació, claus de host, tipus de claus.
* Concepte de fingerprint, autenticació host destí.
* Configuració SSH client
* Configuració SSH servidor.

3. **Configuració client.**

* Practicar configuracions client diverses.
* Documentar les directives de client utilitzades.

4. **Configuració servidor.**

* Practicar configuracions de servidor diverses.
* Documentar les directives de servidor utilitzades.

5. **Accés SSH per clau pública**

* Configurar accés SSH destès per clau pública.
* Utilitzar SSH agent localment.
* Documentar el funcionament de l’accés SSH per clau pública.

## DOCKERFILE
```
# ldapserver
FROM debian:12
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="SSH project"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get  -y install procps iproute2 tree nmap vim less finger passwd  libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils libpam-mount libpam-pwquality openssh-server openssh-client net-tools
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

## COMMON-PASSWORD
```
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
# The "yescrypt" option enables
#hashed passwords using the yescrypt algorithm, introduced in Debian
#11.  Without this option, the default is Unix crypt.  Prior releases
#used the option "sha512"; if a shadow password hash will be shared
#between Debian 11 and older releases replace "yescrypt" with "sha512"
#for compatibility .  The "obscure" option replaces the old
#`OBSCURE_CHECKS_ENAB' option in login.defs.  See the pam_unix manpage
#for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
password        requisite                       pam_pwquality.so retry=3
password        [success=2 default=ignore]      pam_unix.so obscure use_authtok try_first_pass yescrypt
password        [success=1 user_unknown=ignore default=die]     pam_ldap.so use_authtok try_first_pass
# here's the fallback if no module succeeds
password        requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
password        required                        pam_permit.so
# and here are more per-package modules (the "Additional" block)
password        optional        pam_mount.so disable_interactive
# end of pam-auth-update config
```

## COMMON-SESSION
```
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
# The "yescrypt" option enables
#hashed passwords using the yescrypt algorithm, introduced in Debian
#11.  Without this option, the default is Unix crypt.  Prior releases
#used the option "sha512"; if a shadow password hash will be shared
#between Debian 11 and older releases replace "yescrypt" with "sha512"
#for compatibility .  The "obscure" option replaces the old
#`OBSCURE_CHECKS_ENAB' option in login.defs.  See the pam_unix manpage
#for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
password        requisite                       pam_pwquality.so retry=3
password        [success=2 default=ignore]      pam_unix.so obscure use_authtok try_first_pass yescrypt
password        [success=1 user_unknown=ignore default=die]     pam_ldap.so use_authtok try_first_pass
# here's the fallback if no module succeeds
password        requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
password        required                        pam_permit.so
# and here are more per-package modules (the "Additional" block)
password        optional        pam_mount.so disable_interactive
# end of pam-auth-update config
```  

## DOCKER-COMPOSE.YML
```
version: '3'
services:
  ldap:
    image: kiliangp/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    networks:
      - 2hisx
    restart: always

  ssh:
    image: kiliangp/ssh23:base
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    networks:
      - 2hisx
    ports:
      - "2022:22"
    privileged: true
    restart: always

networks:
  2hisx:
```
## LDAP.CONF
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=edt,dc=org
URI	ldap://ldap.edt.org

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

TLS_CACERTDIR /etc/openldap/certs

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on
```

## NSLCD.CONF 
```
# This is the configuration file for the LDAP nameservice
# switch library's nslcd daemon. It configures the mapping
# between NSS names (see /etc/nsswitch.conf) and LDAP
# information in the directory.
# See the manual page nslcd.conf(5) for more information.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The uri pointing to the LDAP server to use for name lookups.
# Multiple entries may be specified. The address that is used
# here should be resolvable without using LDAP (obviously).
#uri ldap://127.0.0.1/
#uri ldaps://127.0.0.1/
#uri ldapi://%2fvar%2frun%2fldapi_sock/
# Note: %2f encodes the '/' used as directory separator
uri ldap://ldap.edt.org

# The LDAP version to use (defaults to 3
# if supported by client library)
#ldap_version 3

# The distinguished name of the search base.
base dc=edt,dc=org

# The distinguished name to bind to the server with.
# Optional: default is to bind anonymously.
#binddn cn=proxyuser,dc=example,dc=com

# The credentials to bind with.
# Optional: default is no credentials.
# Note that if you set a bindpw you should check the permissions of this file.
#bindpw secret

# The distinguished name to perform password modifications by root by.
#rootpwmoddn cn=admin,dc=example,dc=com

# The default search scope.
#scope sub
#scope one
#scope base

# Customize certain database lookups.
#base   group  ou=Groups,dc=example,dc=com
#base   passwd ou=People,dc=example,dc=com
#base   shadow ou=People,dc=example,dc=com
#scope  group  onelevel
#scope  hosts  sub

# Bind/connect timelimit.
#bind_timelimit 30

# Search timelimit.
#timelimit 30

# Idle timelimit. nslcd will close connections if the
# server has not been contacted for the number of seconds.
#idle_timelimit 3600

# Use StartTLS without verifying the server certificate.
#ssl start_tls
#tls_reqcert never

# CA certificates for server certificate verification
#tls_cacertdir /etc/ssl/certs
#tls_cacertfile /etc/ssl/ca.cert

# Seed the PRNG if /dev/urandom is not provided
#tls_randfile /var/run/egd-pool

# SSL cipher suite
# See man ciphers for syntax
#tls_ciphers TLSv1

# Client certificate and key
# Use these, if your server requires client authentication.
#tls_cert
#tls_key

# Mappings for Services for UNIX 3.5
#filter passwd (objectClass=User)
#map    passwd uid              msSFU30Name
#map    passwd userPassword     msSFU30Password
#map    passwd homeDirectory    msSFU30HomeDirectory
#map    passwd homeDirectory    msSFUHomeDirectory
#filter shadow (objectClass=User)
#map    shadow uid              msSFU30Name
#map    shadow userPassword     msSFU30Password
#filter group  (objectClass=Group)
#map    group  member           msSFU30PosixMember

# Mappings for Services for UNIX 2.0
#filter passwd (objectClass=User)
#map    passwd uid              msSFUName
#map    passwd userPassword     msSFUPassword
#map    passwd homeDirectory    msSFUHomeDirectory
#map    passwd gecos            msSFUName
#filter shadow (objectClass=User)
#map    shadow uid              msSFUName
#map    shadow userPassword     msSFUPassword
#map    shadow shadowLastChange pwdLastSet
#filter group  (objectClass=Group)
#map    group  member           posixMember

# Mappings for Active Directory
#pagesize 1000
#referrals off
#idle_timelimit 800
#filter passwd (&(objectClass=user)(!(objectClass=computer))(uidNumber=*)(unixHomeDirectory=*))
#map    passwd uid              sAMAccountName
#map    passwd homeDirectory    unixHomeDirectory
#map    passwd gecos            displayName
#filter shadow (&(objectClass=user)(!(objectClass=computer))(uidNumber=*)(unixHomeDirectory=*))
#map    shadow uid              sAMAccountName
#map    shadow shadowLastChange pwdLastSet
#filter group  (objectClass=group)

# Alternative mappings for Active Directory
# (replace the SIDs in the objectSid mappings with the value for your domain)
#pagesize 1000
#referrals off
#idle_timelimit 800
#filter passwd (&(objectClass=user)(objectClass=person)(!(objectClass=computer)))
#map    passwd uid           cn
#map    passwd uidNumber     objectSid:S-1-5-21-3623811015-3361044348-30300820
#map    passwd gidNumber     objectSid:S-1-5-21-3623811015-3361044348-30300820
#map    passwd homeDirectory "/home/$cn"
#map    passwd gecos         displayName
#map    passwd loginShell    "/bin/bash"
#filter group (|(objectClass=group)(objectClass=person))
#map    group gidNumber      objectSid:S-1-5-21-3623811015-3361044348-30300820

# Mappings for AIX SecureWay
#filter passwd (objectClass=aixAccount)
#map    passwd uid              userName
#map    passwd userPassword     passwordChar
#map    passwd uidNumber        uid
#map    passwd gidNumber        gid
#filter group  (objectClass=aixAccessGroup)
#map    group  cn               groupName
#map    group  gidNumber        gid
# This comment prevents repeated auto-migration of settings.
```
## NSSWITCH.CONF

```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files
gshadow:        files

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```

## PAM_MOUNT.CONF.XML

```
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd">
<!--
	See pam_mount.conf(5) for a description.
-->

<pam_mount>

		<!-- debug should come before everything else,
		since this file is still processed in a single pass
		from top-to-bottom -->

<debug enable="0" />

		<!-- Volume definitions -->

<!--<volume user="*" fstype="tmpfs" mountpoint="~/mytmp" options="size=100M" />
-->
		<!-- pam_mount parameters: General tunables -->

<!--
<luserconf name=".pam_mount.conf.xml" />
-->

<!-- Note that commenting out mntoptions will give you the defaults.
     You will need to explicitly initialize it with the empty string
     to reset the defaults to nothing. -->
<mntoptions allow="nosuid,nodev,loop,encryption,fsck,nonempty,allow_root,allow_other" />
<!--
<mntoptions deny="suid,dev" />
<mntoptions allow="*" />
<mntoptions deny="*" />
-->
<mntoptions require="nosuid,nodev" />

<!-- requires ofl from hxtools to be present -->
<logout wait="0" hup="no" term="no" kill="no" />


		<!-- pam_mount parameters: Volume-related -->

<mkmountpoint enable="1" remove="true" />


</pam_mount>
```

## STARTUP.SH 

```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done


cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml

/usr/sbin/nscd
/usr/sbin/nslcd
mkdir /run/sshd
#sleep infinity
/usr/sbin/sshd -D 
```

## COMPROVACIONS

**VERIFIQUEM QUE PODEN AUTENTICAR-SE ELS USUARIS LOCALS (ELS QUE HEM CREAT AL STARTUP). Tindrem que entrar des d'un usuari UNIX degut a que si entrem per root ens deixarà sense posar cap autenticació, per tant, no estaria bé la verificació.**

```
unix02@ssh:~$ su - unix03
Password: 
unix03@ssh:~$ 
```

**VERIFIQUEM QUE PODEM ACCEDIR ELS USUARIS LDAP. EN AQUEST CAS ACCEDIM A L'USUARI "PERE", PER TANT, HA DE CREAR-SE EL SEU PROPI DIRECTORI HOME**

```
unix03@ssh:~$ su - pere
Password: 
Creating directory '/tmp/home/pere'.
$ bash
pere@ssh:~$ id
uid=5001(pere) gid=601(professors) groups=601(professors)
pere@ssh:~$ pwd
/tmp/home/pere
```

**VERIFICACIÓ DE PORTS**

```
kilian@debian:~/2ASIX/M06/ssh23/ssh23:base$ docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED       STATUS       PORTS                                   NAMES
ed3da2b910fc   kiliangp/ssh23:base      "/bin/sh -c /opt/doc…"   2 hours ago   Up 2 hours   0.0.0.0:2022->22/tcp, :::2022->22/tcp   ssh.edt.org
cc3cd737338f   kiliangp/ldap23:latest   "/bin/sh -c /opt/doc…"   2 hours ago   Up 2 hours   389/tcp                                 ldap.edt.org
```

## SSH COMPROVACIONS

Primerament farem l'ordre "ip a" per mirar quina IP tenim.

```
root@ssh:/opt/docker# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
29: eth0@if30: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:1e:00:03 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.30.0.3/16 brd 172.30.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

Ens conectarem desde fora a l'usuari "anna". Com hem accedit per primera vegada, ens mostrarà el missatge de fingerprint, avisant-nos de que si estem segurs de connectar-nos a aquest usuai que té un token "ztOX9ct9iJTylCzbUIr6rxNALFWc4Ho65zDbXti".

```
kilian@debian:~/2ASIX/M06/ssh23/ssh23:base$ ssh anna@172.30.0.3
The authenticity of host '172.30.0.3 (172.30.0.3)' can't be established.
ECDSA key fingerprint is SHA256:ztOX9ct9iJTylCzbUIr6rxNALFWc4Ho65zDbXti/+hA.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.30.0.3' (ECDSA) to the list of known hosts.
anna@172.30.0.3's password: 
Creating directory '/tmp/home/anna'.
Linux ssh.edt.org 6.1.0-0.deb11.13-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1~bpo11+1 (2023-10-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$ 
```

Si tornem a connectar-nos, podem veure que el missatge sobre el token no apareix i demana directamente el password, això passa degut a que s'ha guardat el fitxer ~/.ssh/known_hosts

```
kilian@debian:~$ ssh anna@172.30.0.3
anna@172.30.0.3's password: 
Linux ssh.edt.org 6.1.0-0.deb11.13-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1~bpo11+1 (2023-10-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 12:37:06 2024 from 172.30.0.1
$ 


kilian@debian:~$ cat ~/.ssh/known_hosts 
|1|l7R/wUYi/7h5RaKXrF7lhKhJ8XA=|g9CAIZLfcmPZGVMzxpyFFw76S04= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIIg0TSFPjjskbJZ1EJsSgyAkX/dpKKfhS4TfskWF7OmdLUEDBHtasqx1nCBr3CILmn+ItcLLGhl1AJc1dQTFtk=

```

## PROVA MAN-IN-THE-MIDDLE

Afegirem una resolució de nom de domini al fitxer **/etc/hosts**, per fer una prova de connexió amb una paraula. Ens estarem connectant des de
```
root@ssh:/opt/docker# cat /etc/hosts
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.30.0.3	ssh.edt.org ssh
127.0.0.1 vinibaila

root@ssh:/opt/docker# nmap vinibaila
Starting Nmap 7.93 ( https://nmap.org ) at 2024-02-07 12:41 UTC
Nmap scan report for vinibaila (127.0.0.1)
Host is up (0.0000020s latency).
rDNS record for 127.0.0.1: localhost
Not shown: 999 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.09 seconds
```

Per tant, ara accedirem per ssh cap una resolució de nom. Podem veure que ens connecta sense problemes, gràcies a que hem propagat el port per totes les interfícies.

```
root@ssh:/opt/docker# ssh anna@vinibaila
The authenticity of host 'vinibaila (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:ihC2Wc5hr0HTlEenw4Iwdip9mU/MLXCSXk+cl0i0i1M.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'vinibaila' (ED25519) to the list of known hosts.
anna@vinibaila's password: 
Linux ssh.edt.org 6.1.0-0.deb11.13-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1~bpo11+1 (2023-10-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 12:37:54 2024 from 172.30.0.1
$ 
```
Ara, perquè ens doni l'error de Man-in-the-middle, canviarem la direcció IP del fitxer /etc/hosts.

```
root@ssh:/opt/docker# cat /etc/hosts
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.30.0.3	ssh.edt.org ssh
127.10.0.1 vinibaila -> canviarem la direcció de 127.0.0.1 a 127.10.0.1
```

I ens tornarem a connectar un altre cop.

```
root@ssh:/opt/docker# ssh anna@vinibaila
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@	WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! 	@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:FyEIqfKp1l24TVvD742V8a7k5J5+JSgwsHvlei6JNwc.
Please contact your system administrator.
Add correct host key in /root/.ssh/known_hosts to get rid of this message.
Offending ED25519 key in /root/.ssh/known_hosts:1
  remove with:
  ssh-keygen -f "/root/.ssh/known_hosts" -R "vinibaila"
Host key for vinibaila has changed and you have requested strict checking.
Host key verification failed.
```
## CONFIGURACIÓ CLIENT

Ens indica que no es pot connectar a l'usuari unix02 del localhost, degut a que no hi ha cap usuari a la màquina física, a més hem afegit uns paràmetres per visualitzar el HostKey.
```
root@ssh:/opt/docker# ssh -o VisualHostKey=yes -o PasswordAuthentication=no unix02@localhost
The authenticity of host 'localhost (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:ihC2Wc5hr0HTlEenw4Iwdip9mU/MLXCSXk+cl0i0i1M.
+--[ED25519 256]--+
|  + +.o*+o..     |
| o =.%oo*+o      |
|. =.@.B+E.       |
| o @.* =.o       |
|  + + = S        |
|   . + o         |
|    o .          |
|                 |
|                 |
+----[SHA256]-----+
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:1: [hashed name]
    ~/.ssh/known_hosts:4: [hashed name]
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'localhost' (ED25519) to the list of known hosts.
unix02@localhost: Permission denied (publickey,password).
```

## FITXER /ETC/SSH/SSH_CONFIG

Podem implementar l'exemple anterior a través de la configuració del arxiu, per tant, quedaria de la següent manera:


```
root@ssh:/opt/docker# vim /etc/ssh/ssh_config

PasswordAuthentication no
VisualHostKey yes


root@ssh:/opt/docker# ssh unix02@localhost
Host key fingerprint is SHA256:ihC2Wc5hr0HTlEenw4Iwdip9mU/MLXCSXk+cl0i0i1M
+--[ED25519 256]--+
|  + +.o*+o..     |
| o =.%oo*+o      |
|. =.@.B+E.       |
| o @.* =.o       |
|  + + = S        |
|   . + o         |
|    o .          |
|                 |
|                 |
+----[SHA256]-----+
unix02@localhost: Permission denied (publickey,password).

```

## SSH DESATÈS PER CLAU PÚBLICA

Generem una clau pública i privada perquè l'usuari **anna** pugui accedir sense cap autenticació

```
kilian@debian:~/.ssh$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/kilian/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/kilian/.ssh/id_rsa
Your public key has been saved in /home/kilian/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:nL52x4sF67DT4qu/7UEnTlMdk4W14gJVWqviIK9EeRw kilian@debian
The key's randomart image is:
+---[RSA 3072]----+
|           ..oo=o|
|          . o.+o.|
|       E . ..o.. |
|      o.....o .  |
|     + +S.*o..   |
|    . +.o+.*.    |
|     . .oo+..    |
|    . . +*o+o    |
|     ..=BB=...   |
+----[SHA256]-----+

kilian@debian:~/.ssh$ ls -la -> podem veure que la clau pública (.pub) i privada s'ha afegit correctamente
total 20
drwx------  2 kilian kilian 4096 Feb  7 14:08 .
drwxr-xr-x 26 kilian kilian 4096 Feb  7 11:39 ..
-rw-------  1 kilian kilian 2602 Feb  7 14:08 id_rsa
-rw-r--r--  1 kilian kilian  567 Feb  7 14:08 id_rsa.pub

kilian@debian:~/.ssh$ ssh-copy-id -i /home/kilian/.ssh/id_rsa.pub anna@172.30.0.3 -> pasarem el fitxer de clau pública dintre del nostre contenidor, per tant, farem ús de l'ordre "ssh-copy-id".

/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/kilian/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
anna@172.30.0.3's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'anna@172.30.0.3'"
and check to make sure that only the key(s) you wanted were added.

--> Automàticament, es crearà un fitxer d'accés que estarà a ~/.ssh/authorized_keys 
```

## MOSTRA DEL FITXER AUTHORIZED

Podem veure que s'ha creat el fitxer automàticament

```
anna@ssh:~$ ls -l ~/.ssh/authorized_keys 
-rw------- 1 anna professors 567 Feb  7 13:12 /tmp/home/anna/.ssh/authorized_keys

kilian@debian:~/.ssh$ ssh anna@172.30.0.3 -> accedim des de l'ordinador local cap a l'usuari anna, podem veure que no demana contrasenya.
Linux ssh.edt.org 6.1.0-0.deb11.13-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1~bpo11+1 (2023-10-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 12:51:26 2024 from 127.0.0.1
$ 
```
Podem crear un parell de claus amb l'algorime ECSDA de la següent manera:

```
kilian@debian:~/.ssh$ ssh-keygen -t ecdsa
Generating public/private ecdsa key pair.
Enter file in which to save the key (/home/kilian/.ssh/id_ecdsa): vini  -> veiem que l'hem posat la paraula vini
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in vini
Your public key has been saved in vini.pub
The key fingerprint is:
SHA256:EyFqP4SaS8VPH2J3U+5Rt7sCBxWlQ45cm68i7nwvWwQ kilian@debian
The key's randomart image is:
+---[ECDSA 256]---+
|      . .   .=+..|
|   . o . ..o*.+..|
|    * = + oEo* . |
|   = * + + oo.o .|
|  +   + S  ..o o |
| . .   . .  + . .|
|  .      . . + . |
|        o .oo .  |
|        .+..+.   |
+----[SHA256]-----+


kilian@debian:~/.ssh$ ssh-copy-id -i /home/kilian/.ssh/vini.pub pau@172.30.0.3 -> copiem el fitxer vini.pub dintre del container.
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/kilian/.ssh/vini.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'pau@172.30.0.3'"
and check to make sure that only the key(s) you wanted were added.

kilian@debian:~/.ssh$ ssh pau@172.30.0.3
Linux ssh.edt.org 6.1.0-0.deb11.13-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1~bpo11+1 (2023-10-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$ 
```

## CONFIGURACIÓ SERVIDOR

Editarem el fitxer /etc/ssh/sshd_conf

```
root@ssh:~/.ssh# vim /etc/ssh/ssh_config
Port 2022 -> propagamos por el puerto 2022
#AddressFamily any
ListenAddress 0.0.0.0 -> propagamos por TODAS las interfícies
#ListenAddress ::

root@ssh:~/.ssh# ps ax 
    PID TTY      STAT   TIME COMMAND
      1 ?        Ss     0:00 /bin/sh -c /opt/docker/startup.sh
      7 ?        S      0:00 /bin/bash /opt/docker/startup.sh
     59 ?        Ssl    0:00 /usr/sbin/nscd
     72 ?        Sl     0:00 /usr/sbin/nslcd
     79 ?        S      0:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 star
    348 pts/0    Ss     0:00 /bin/bash
    520 pts/0    R+     0:00 ps ax
root@ssh:~/.ssh# kill -1 79 -> reiniciamos el servicio.

root@ssh:~/.ssh# nmap 127.0.0.1
Starting Nmap 7.93 ( https://nmap.org ) at 2024-02-07 13:47 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000020s latency).
Not shown: 999 closed tcp ports (reset)
PORT   STATE SERVICE
2022/tcp open  ssh
```

Dintre del fitxer /etc/ssh/sshd_config
```
Match User pau
    banner /etc/benvinguda
    PasswordAuthentication no

kilian@debian:~/.ssh$ ssh pau@172.30.0.3
Benvinguts el servei SSH
pau@172.30.0.3: Permission denied (publickey).
```

Com a últim exemple farem ús del paràmetre "AllowUsers"
```
vim /etc/ssh/sshd_config
AllowUsers unix02 marta


kilian@debian:~/.ssh$ ssh pau@172.30.0.3
pau@172.30.0.3's password:
Permission denied, please try again.
pau@172.30.0.3's password:

kilian@debian:~/.ssh$ ssh marta@172.30.0.3
marta@172.30.0.3's password:
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
marta@ssh:~$ exit
logout
Connection to 172.30.0.3 closed.
```

