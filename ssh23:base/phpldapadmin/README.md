# M06. ADMINISTRACIÓ AVANÇADA DE SISTEMES OPERATIUS

## DOCKER-COMPOSE.YML

```
version: "3"
services:
  ldap:
    image: kiliangp/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  ssh:
    image: kiliangp/ssh23:base
    privileged: true
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    ports:
      - 2022:20
    networks:
      - 2hisx
  phpldapadmin:
    image: kiliangp/phpldapadmin:latest
    container_name: phpldapadmin
    hostname: phpldapadmin
    ports:
      - "80:80"
    networks:
      - 2hisx

networks:
  2hisx:
```

**Accedirem al navegador amb la direcció URL http://localhost/phpldapadmin**

Una vegada en aquest punt, posarem les següents credencials:

**Login DN -> cn=Manager,dc=edt,dc=org**

**Password -> secret**

## PROVES LDAP I USUARIS LOCALS

```
root@ssh:/opt/docker# getent passwd pau -> usuari LDAP
pau:*:5000:600:Pau Pou:/tmp/home/pau:

root@ssh:/opt/docker# getent passwd unix02 -> usuari LOCAL
unix02:x:1001:1001::/home/unix02:/bin/bash

```
